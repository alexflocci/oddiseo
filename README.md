# Oddiseo Corp KSP missions

Missions for Kerbal Space Program with an emphasis on in-situ repair and rescue. Requires Contract Configurator to run, and KIS (Kerbal Inventory System) in order to actually complete the missions.

## Manual installation

* Make sure Contract Configurator is installed
* Make sure Kerbal Inventory System is installed
* Download the entire contents of this repository into `<KSP directory>/GameData/ContractPacks/Oddiseo`

Oddiseo contract pack is released under the [GPL 3.0 licence](licence.md).